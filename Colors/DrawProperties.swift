//
//  DrawProperties.swift
//  Colors
//
//  Created by Thomas Muzy on 01/06/2017.
//  Copyright © 2017 Thomas Muzy. All rights reserved.
//

import UIKit

struct DrawProperties {
    var opacity: CGFloat
    var color: UIColor
    var width: CGFloat
}
