//
//  Canvas.swift
//  Colors
//
//  Created by Thomas Muzy on 30/05/2017.
//  Copyright © 2017 Thomas Muzy. All rights reserved.
//

import UIKit

class Canvas: UIView {
    
    var color: UIColor = UIColor.black
    var bgColor: UIColor = UIColor.white
    var brushWidth: CGFloat = 5.0
    var opacity:CGFloat = 1.0
    
    
    private var slate: UIImage?
    
    private var drawing = false
    
    private var bezierPath:UIBezierPath = UIBezierPath()
    private var bezierPoints = [CGPoint!](repeating: nil, count: 5)
    private var bezierPointIndex:Int!
    
    private var bezierPathHistory:[Curve] = []
    private var bezierPathRedoHistory:[Curve] = []

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupGestureRecognizers()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupGestureRecognizers()
    }
    
    private func setupGestureRecognizers(){
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleGesture(sender:)))
        panRecognizer.maximumNumberOfTouches = 1
        self.addGestureRecognizer(panRecognizer)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tapRecognizer.numberOfTouchesRequired = 1
        self.addGestureRecognizer(tapRecognizer)
    }
    
    @objc private func handleGesture(sender: UIPanGestureRecognizer){
        let point = sender.location(in: self)
        switch sender.state{
        case .began:
            startAtPoint(point)
        case .changed:
            continueAtPoint(point)
        case .ended:
            endAtPoint(point)
        case .failed:
            endAtPoint(point)
        default:
            assert(false, "State not handled")
        }
    }
    
    @objc private func handleTap(sender: UITapGestureRecognizer){
        let point = sender.location(in: self)
        if sender.state == .ended {
            tapAtPoint(point)
        }
    }
   
    //configure l'UIBezierPath pour l'afficher à l'écran
    private func drawCurve(from: CGPoint, to: CGPoint, ctr1: CGPoint, ctr2: CGPoint){

        bezierPath.move(to: from)
        bezierPath.addCurve(to: to, controlPoint1: ctr1, controlPoint2: ctr2)
        bezierPath.lineWidth = brushWidth * 2.0
        //drawInContext()
    }
    
    //configure l'UIBezierPath pour l'afficher à l'écran
    private func drawPoint(at: CGPoint){
        bezierPath = UIBezierPath(arcCenter: at, radius: brushWidth/2, startAngle: 0, endAngle: 2 * CGFloat(Double.pi), clockwise: true)
        bezierPath.lineWidth = brushWidth
//        drawInContext()
    }
    
    
    
    override func draw(_ rect: CGRect) {
        if !drawing {
         slate?.draw(in: rect)
        } else {
            if let temp = slate?.cgImage?.cropping(to: rect) {
                UIImage(cgImage: temp).draw(in: rect)
            }
            
        }
        
        
        color.setStroke()

        bezierPath.stroke(with: .normal, alpha: opacity)

        
    }
    
    //commit le tracé au canvas
    private func drawInContext(){
        UIGraphicsBeginImageContext(self.bounds.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(bgColor.cgColor)
        context?.fill(self.bounds)
        slate?.draw(in: self.bounds)
        
        color.setStroke()
        
        bezierPath.flatness = 1

        bezierPath.lineCapStyle = .round
        bezierPath.stroke(with: CGBlendMode.normal, alpha: opacity)
        
        slate = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    func startAtPoint(_ point: CGPoint) {
        bezierPointIndex = 0
        bezierPoints[0] = point
        bezierPath.flatness = 5
        bezierPath.lineCapStyle = .round
        drawing = true
    }
    
    func continueAtPoint(_ point: CGPoint) {
        autoreleasepool {
            bezierPointIndex = bezierPointIndex + 1
            bezierPoints[bezierPointIndex] = point
            if bezierPointIndex == 4{
                
                //destination de la courbe calculée pour éviter une cassure
                let x = (bezierPoints[2].x + bezierPoints[4].x)/2.0
                let y = (bezierPoints[2].y + bezierPoints[4].y)/2.0
                bezierPoints[3] = CGPoint(x: x, y: y)
                
                //met à jour l'affichage avec la courbe de bézier actuelle
                drawCurve(from: bezierPoints[0], to: bezierPoints[3], ctr1: bezierPoints[1], ctr2: bezierPoints[2])
                let drawBounds = CGRect(x: bezierPath.bounds.minX - brushWidth, y: bezierPath.bounds.minY - brushWidth, width: bezierPath.bounds.width + brushWidth*2, height: bezierPath.bounds.height + brushWidth*2)
                
                setNeedsDisplay(drawBounds)
                //print(bezierPath)
                //décalage des points pour la prochaine courbe
                bezierPoints[0] = bezierPoints[3];
                bezierPoints[1] = bezierPoints[4];
                bezierPointIndex = 1;
            }
        }
    }
    
    func endAtPoint(_ point: CGPoint) {
        drawing = false
        drawInContext()
        self.setNeedsDisplay()
        bezierPathRedoHistory = []
        bezierPathHistory.append(Curve(type: Curve._type.curve , bezier: UIBezierPath(cgPath: bezierPath.cgPath), opacity: opacity, color: color, width: brushWidth))
        bezierPath.removeAllPoints()
    }
    
    func tapAtPoint(_ point: CGPoint){
        autoreleasepool{
            drawPoint(at: point)
            self.setNeedsDisplay()
            drawInContext()
            bezierPathRedoHistory = []
            bezierPathHistory.append(Curve(type: Curve._type.circle, bezier: UIBezierPath(cgPath: bezierPath.cgPath), opacity: opacity, color: color, width: brushWidth))
            bezierPath.removeAllPoints()
        }
   }
    
    func clear(){
        slate = nil
        bezierPath.removeAllPoints()
        bezierPathHistory.append(Curve(type: Curve._type.clear, bezier: UIBezierPath(), opacity: 0, color: color, width: brushWidth))

        self.setNeedsDisplay()
    }
    
    func undo(){
        slate = nil
        bezierPath.removeAllPoints()
        let savedProperties = DrawProperties(opacity: opacity, color: color, width: brushWidth)
        if bezierPathHistory.count > 0 {
            bezierPathRedoHistory.append(bezierPathHistory.popLast()!)
            for bp in bezierPathHistory{
                bezierPath = UIBezierPath(cgPath: bp.bezier.cgPath)
                color = bp.color
                opacity = bp.opacity
                bezierPath.lineWidth = bp.width
                if bp.type == .curve {
                    bezierPath.lineWidth = bezierPath.lineWidth * 2.0
                    drawInContext()
                }
                if bp.type == .circle {
                    drawInContext()
                }
                if bp.type == .clear {
                    slate = nil
                }
                drawInContext()
            }
        }
        self.setNeedsDisplay()
        bezierPath.removeAllPoints()
        color = savedProperties.color
        brushWidth = savedProperties.width
        opacity = savedProperties.opacity
        
    }
    
    
    func redo(){
        //print(bezierPathRedoHistory.count)
        let savedProperties = DrawProperties(opacity: opacity, color: color, width: brushWidth)
        if bezierPathRedoHistory.count > 0 {
            
            let bp = bezierPathRedoHistory.last!
            bezierPath = UIBezierPath(cgPath: bp.bezier.cgPath)
            color = bp.color
            opacity = bp.opacity
            bezierPath.lineWidth = bp.width
            if bp.type == .curve {
                bezierPath.lineWidth = bezierPath.lineWidth * 2.0
                drawInContext()
            }
            if bp.type == .circle {
                drawInContext()
            }
            if bp.type == .clear {
                slate = nil
            }

            bezierPathHistory.append(bezierPathRedoHistory.popLast()!)
        }
        self.setNeedsDisplay()
        bezierPath.removeAllPoints()
        color = savedProperties.color
        brushWidth = savedProperties.width
        opacity = savedProperties.opacity
    }
    
    func getCanvas() -> UIImage? {
        return slate
    }
    

}
