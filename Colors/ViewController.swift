//
//  ViewController.swift
//  Colors
//
//  Created by Thomas Muzy on 30/05/2017.
//  Copyright © 2017 Thomas Muzy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, HSBColorPickerDelegate, DraggableViewDelegate{
    
    //Outlets
    @IBOutlet weak var ui_mainCanvas: Canvas!
    @IBOutlet weak var ui_previewCanvas: Canvas!
    @IBOutlet weak var ui_widthSlider: UISlider!
    @IBOutlet weak var ui_opacitySlider: UISlider!
    @IBOutlet weak var ui_optionsView: OptionsView!
    @IBOutlet weak var ui_toolbar: UIToolbar!
    @IBAction func ui_widthSliderValueChanged(_ sender: UISlider) {
        ui_mainCanvas.brushWidth = CGFloat(sender.value)
        ui_previewCanvas.brushWidth = CGFloat(sender.value)
        ui_previewCanvas.undo()
        ui_previewCanvas.tapAtPoint(CGPoint(x: 50, y: 50))
    }
    @IBAction func ui_opacitySliderValueChanged(_ sender: UISlider) {
        ui_mainCanvas.opacity = CGFloat(sender.value)
        ui_previewCanvas.opacity = CGFloat(sender.value)
        ui_previewCanvas.undo()
        ui_previewCanvas.tapAtPoint(CGPoint(x: 50, y: 50))
    }

    @IBAction func ui_buttonClearPress(_ sender: UIButton) {
        ui_mainCanvas.clear()
    }
    
    @IBAction func ui_buttonUndoPress(_ sender: UIButton) {
        ui_mainCanvas.undo()
    }

    @IBAction func ui_buttonRedoPress(_ sender: UIButton) {
        ui_mainCanvas.redo()
    }
    @IBOutlet weak var ui_draggableView: DraggableView!
    
    @IBOutlet weak var ui_colorPicker: HSBColorPicker!
    @IBAction func ui_saveButtonPress(_ sender: UIButton) {
        saveCanvas() 
    }
    //
    
    //attributes
    //var colorArray = [ 0x000000, 0xfe0000, 0xff7900, 0xffb900, 0xffde00, 0xfcff00, 0xd2ff00, 0x05c000, 0x00c0a7, 0x0600ff, 0x6700bf, 0x9500c0, 0xbf0199, 0xffffff ]
    let colorArray = [ 0x000000, 0x262626, 0x4d4d4d, 0x666666, 0x808080, 0x990000, 0xcc0000, 0xfe0000, 0xff5757, 0xffabab, 0xffabab, 0xffa757, 0xff7900, 0xcc6100, 0x994900, 0x996f00, 0xcc9400, 0xffb900, 0xffd157, 0xffe8ab, 0xfff4ab, 0xffe957, 0xffde00, 0xccb200, 0x998500, 0x979900, 0xcacc00, 0xfcff00, 0xfdff57, 0xfeffab, 0xf0ffab, 0xe1ff57, 0xd2ff00, 0xa8cc00, 0x7e9900, 0x038001, 0x04a101, 0x05c001, 0x44bf41, 0x81bf80, 0x81c0b8, 0x41c0af, 0x00c0a7, 0x00a18c, 0x00806f, 0x040099, 0x0500cc, 0x0600ff, 0x5b57ff, 0xadabff, 0xd8abff, 0xb157ff, 0x6700bf, 0x5700a1, 0x450080, 0x630080, 0x7d00a1, 0x9500c0, 0xa341bf, 0xb180bf, 0xbf80b2, 0xbf41a6, 0xbf0199, 0xa10181, 0x800166, 0x999999, 0xb3b3b3, 0xcccccc, 0xe6e6e6, 0xffffff]
    
    var menuOpen:Bool = false
    //
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ui_draggableView.layer.cornerRadius = 30
        
        ui_colorPicker.delegate = self
        ui_draggableView.delegate = self
        
        ui_draggableView.image =  #imageLiteral(resourceName: "iconmonstr-paintbrush-7-240")
        ui_previewCanvas.tapAtPoint(CGPoint(x: 50, y:50))
        ui_previewCanvas.layer.borderWidth = 1
        ui_previewCanvas.layer.borderColor = UIColor.black.cgColor
    }
    
    func HSBColorColorPickerTouched(sender:HSBColorPicker, color:UIColor, point:CGPoint, state:UIGestureRecognizerState){
        ui_mainCanvas.color = color
        if menuOpen {
            ui_draggableView.backgroundColor = UIColor(cgColor: color.cgColor.copy(alpha: 0.4)!)
        }
        ui_previewCanvas.color = color
        ui_previewCanvas.undo()
        ui_previewCanvas.tapAtPoint(CGPoint(x: 50, y:50))
    }
    
    
    func viewDragged(sender: DraggableView, point: CGPoint, gestureState: UIGestureRecognizerState){
        let origin = ui_draggableView.frame.origin
        var newPos = CGPoint(x: origin.x + point.x, y: origin.y + point.y)
        UIView.animate(withDuration: 0.2) {
            self.ui_draggableView.center = newPos
        }
        
        if gestureState == .began {
             sender.backgroundColor = UIColor(cgColor: self.ui_mainCanvas.color.cgColor.copy(alpha: 0.3)!)
        }
        
        
        if gestureState == .ended {
            if newPos.x > self.view.center.x {
                newPos.x = self.view.frame.width - self.ui_draggableView.frame.width/2.0

            } else {
                newPos.x =  self.ui_draggableView.frame.width/2.0
                
            }
            
            if newPos.y + self.ui_draggableView.frame.height/2.0 > self.view.frame.height - self.ui_toolbar.frame.height {
                
                newPos.y = (self.view.frame.height - self.ui_toolbar.frame.height) - self.ui_draggableView.frame.height/2.0
            }
            if newPos.y - self.ui_draggableView.frame.height/2.0 < 0 {
                newPos.y = self.ui_draggableView.frame.height/2.0

            }
            

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveEaseInOut , animations: {
                self.ui_draggableView.center = newPos
            }, completion: nil)
            
            sender.backgroundColor = UIColor.clear
        }
    }
    
    func viewTapped(sender: DraggableView, point: CGPoint) {
        menuOpen = !menuOpen
        ui_optionsView.isHidden = !ui_optionsView.isHidden
        if menuOpen {
            
            sender.backgroundColor = UIColor(cgColor: self.ui_mainCanvas.color.cgColor.copy(alpha: 0.3)!)
        } else {
            sender.backgroundColor = UIColor.clear
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func hexToUIColor(_ rgbValue: Int) -> UIColor {
        let red =   CGFloat((rgbValue & 0xFF0000) >> 16) / 0xFF
        let green = CGFloat((rgbValue & 0x00FF00) >> 8) / 0xFF
        let blue =  CGFloat(rgbValue & 0x0000FF) / 0xFF
        let alpha = CGFloat(1.0)
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    

    func saveCanvas(){
        if let saveImage = self.ui_mainCanvas.getCanvas() {
            let fileManager = FileManager.default
            let imageData = UIImagePNGRepresentation(saveImage)
            if let path = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
                
                let alert = UIAlertController.init(title: "Save", message: "Do you want to save this masterpiece ?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    let id = Date.init().hashValue
                    let imagePath = path.appendingPathComponent("\(id)")
                    print(imagePath)
                    try? imageData?.write(to: imagePath, options: .atomic)
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: nil))
                
                self.present(alert, animated: false, completion: nil)
            }
        }
    }
    
    func openSavedCanvas(){
        //
    }

}

