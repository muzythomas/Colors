//
//  OptionsView.swift
//  Colors
//
//  Created by Thomas Muzy on 02/06/2017.
//  Copyright © 2017 Thomas Muzy. All rights reserved.
//

import UIKit

class OptionsView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        self.addGestureRecognizer(tapRecognizer)
    }
    
    @objc private func handleTap(sender: UITapGestureRecognizer){
        self.isHidden = true 
    }
}
