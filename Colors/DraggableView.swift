//
//  DraggableView.swift
//  Colors
//
//  Created by Thomas Muzy on 02/06/2017.
//  Copyright © 2017 Thomas Muzy. All rights reserved.
//

import UIKit

internal protocol DraggableViewDelegate {
    func viewDragged(sender: DraggableView, point: CGPoint, gestureState: UIGestureRecognizerState)
    func viewTapped(sender: DraggableView, point: CGPoint)
}

class DraggableView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var pPoint:CGPoint = CGPoint.zero
    internal var delegate: DraggableViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupGestureRecognizers()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupGestureRecognizers()
    }
    
    private func setupGestureRecognizers(){
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleGesture(sender:)))
        self.addGestureRecognizer(panRecognizer)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        self.addGestureRecognizer(tapRecognizer)
    }
    
    @objc private func handleGesture(sender: UIPanGestureRecognizer){
        let point = sender.location(in: self)
        self.delegate?.viewDragged(sender: self, point: point, gestureState: sender.state)
    }
    
    @objc private func handleTap(sender: UITapGestureRecognizer){
        let point = sender.location(in: self)
        if sender.state == .ended {
            self.delegate?.viewTapped(sender: self, point: point)
        }
    }
    
    
}
