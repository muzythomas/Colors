//
//  Curve.swift
//  Colors
//
//  Created by Thomas Muzy on 01/06/2017.
//  Copyright © 2017 Thomas Muzy. All rights reserved.
//

import UIKit

struct Curve {
    enum _type {
        case circle
        case curve
        case clear
    }
    var type:_type
    var bezier:UIBezierPath
    var opacity: CGFloat
    var color: UIColor
    var width: CGFloat 
}
